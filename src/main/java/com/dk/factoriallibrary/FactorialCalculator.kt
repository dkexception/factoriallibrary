package com.dk.factoriallibrary

/**
 * Singleton class [FactorialCalculator]
 */
object FactorialCalculator {
    /**
     * This method will calculate factorial for [n]
     * @param n int for which factorial is to be calculated
     */
    fun calculateFactorial(n: Int): Int = if (n == 0) 1
    else n * calculateFactorial(n - 1)
}